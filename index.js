/** Bài 1
 * Input: số ngày làm
 *
 * Step:
 * 1. tạo ra 2 biến chứa tiền lương 1 ngày và số ngày làm
 * 2. áp dụng lương = lương 1 ngày * số ngày
 *
 *
 * Output: lương
 */
function tinhLuong() {
  var luongValue = document.getElementById("luong-mot-ngay").value * 1;
  var soNgayLamValue = document.getElementById("so-ngay-lam").value * 1;
  var tienLuong = luongValue * soNgayLamValue;
  console.log("Tiền lương: ", tienLuong);
  document.getElementById(
    "tien-luong"
  ).innerHTML = `Tiền lương bạn được nhận: ${tienLuong.toLocaleString()} VND`;
}

/** Bài 2
 * Input: giá trị của 5 số
 *
 * Step:
 * 1. tạo ra  biến 5 số
 * 2. áp dụng tính trung bình
 *
 *
 * Output: số trung bình
 */

function tinhSoTrungBinh() {
  var soThuNhatValue = document.getElementById("so-thu-nhat").value * 1;
  var soThuHaiValue = document.getElementById("so-thu-hai").value * 1;
  var soThuBaValue = document.getElementById("so-thu-ba").value * 1;
  var soThuTuValue = document.getElementById("so-thu-tu").value * 1;
  var soThuNamValue = document.getElementById("so-thu-nam").value * 1;
  //   console.log({ soThuNhat, soThuHai, soThuBa, soThuTu, soThuNam });
  var soTb =
    (soThuNhatValue +
      soThuHaiValue +
      soThuBaValue +
      soThuTuValue +
      soThuNamValue) /
    5;
  console.log("Số trung bình: ", soTb);
  document.getElementById(
    "so-trung-binh"
  ).innerHTML = `Số trung bình: ${soTb.toFixed(2)}`;
}

/** Bài 3
 * Input: số tiền USD
 *
 * Step:
 * 1. tạo ra 2 biến số
 * 2. áp dụng tỉ giá quy đổi
 *
 *
 * Output: số tiền VND
 */

function tinhTienQuyDoi() {
  var soTienUsdValue = document.getElementById("so-tien-usd").value * 1;
  var tyGiaValue = document.getElementById("ty-gia").value * 1;
  var soTienVnd = soTienUsdValue * tyGiaValue;
  console.log("Số Tiền VND: ", soTienVnd);
  document.getElementById(
    "quy-doi-tien"
  ).innerHTML = `Số tiền VND: ${new Intl.NumberFormat("vn-VN").format(
    soTienVnd
  )} VND`;
}

/** Bài 4
 * Input: giá trị chiều dài, chiều rộng
 *
 * Step:
 * 1. tạo ra 2 biến số chiều dài, chiều rộng
 * 2. áp dụng công thức tính diện tích và chu vi
 *
 *
 * Output: chu vi, diện tích
 */

function tinhCongThucHcn() {
  var chieuDaiValue = document.getElementById("chieu-dai").value * 1;
  var chieuRongValue = document.getElementById("chieu-rong").value * 1;
  var chuVi = (chieuDaiValue + chieuRongValue) * 2;
  var dienTich = chieuDaiValue * chieuRongValue;
  console.log({ chuVi, dienTich });
  document.getElementById(
    "ket-qua"
  ).innerHTML = `Chu vi: ${chuVi}, Diện tích: ${dienTich}`;
}

/** Bài 5
 * Input: số có 2 chữ số
 *
 * Step:
 * 1. tạo ra 2 biến số của hàng đơn vị, hàng chục
 * 2. tính các hàng đơn vị, chục
 *
 *
 * Output: tổng 2 số
 */

function tinhTong() {
  var soNhapVaoValue = document.getElementById("so-nhap-vao").value * 1;
  var soHangDonVi = soNhapVaoValue % 10;
  var soHangChuc = Math.floor((soNhapVaoValue % 100) / 10);
  var tong2So = soHangDonVi + soHangChuc;
  console.log("Tổng 2 ký sô: ", tong2So);
  document.getElementById(
    "tong-2-chu-so"
  ).innerHTML = `Tổng 2 ký số: ${tong2So}`;
}
